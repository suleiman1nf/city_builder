using NUnit.Framework;

namespace Tests.EditMode
{
    public class GeneratorTest
    {
        Generator generator = new Generator(10, 10, 1, 1, 0.05f, 0.05f);
        
        [Test]
        public void GeneratorTestSwampPercent()
        {
            var data = generator.Generate();
            int swampCount = 0;
            foreach (var tileData in data.tilesList)
            {
                if (tileData.TileType == TileType.Swamp)
                    swampCount++;
            }
            Assert.AreEqual(5,swampCount);
        }
        
        [Test]
        public void GeneratorTestWaterPercent()
        {
            var data = generator.Generate();
            int waterCount = 0;
            foreach (var tileData in data.tilesList)
            {
                if (tileData.TileType == TileType.Water)
                    waterCount++;
            }
            Assert.AreEqual(5,waterCount);
        }
    }
}
