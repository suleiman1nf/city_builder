using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace Tests.EditMode
{
    public class BuildingsGeneratorTest
    {
        [Test]
        public void BuildingsGeneratorTestBuildingsCount()
        {

            List<TileData> tiles = new List<TileData>();
            int buildableCount = 90;
            for (int i = 0; i < buildableCount; i++)
            {
                tiles.Add(new TileData{isBuildable = true});
            }
            int nonBuildableCount = 10;
            for (int i = 0; i < nonBuildableCount; i++)
            {
                tiles.Add(new TileData{isBuildable = false});
            }

            TileData[,] tilesArray = new TileData[10, 10];
            int index = 0;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    tilesArray[i, j] = tiles[index];
                    index++;
                }
            }
            
            GroundData groundData = new GroundData{tilesList = tiles, tilesArray = tilesArray};

            BuildingsGenerator buildingsGenerator = new BuildingsGenerator(0.1f);
            buildingsGenerator.Generate(groundData);

            int buildingsCount = tiles.Count(tile => tile.hasBuilding);
            
            Assert.AreEqual(9, buildingsCount);
        }
    }
}
