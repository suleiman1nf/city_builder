﻿using System.Collections.Generic;
using UnityEngine;

public class GroundPlacer : MonoBehaviour
{
    [SerializeField] private Transform groundsParent;

    private string resourcesPath = "Grounds/";
    
    public void Place(List<TileData> grounds)
    {
        foreach (Transform child in groundsParent)
        {
            Destroy(child.gameObject);
        }
        
        foreach (var ground in grounds)
        {
            Create(ground);
        }
    }

    public void Create(TileData tile)
    {
        GameObject prefab = Resources.Load<GameObject>(resourcesPath + tile.ResourcesName);
        GameObject go = Instantiate(prefab, groundsParent);
        go.GetComponent<Ground>().tile = tile;
        go.transform.localPosition = tile.Position;
        go.transform.localEulerAngles = tile.Rotation;
    }
    
}