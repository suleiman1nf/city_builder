﻿using UnityEngine;

public abstract class SaveController
{
    protected string fileName = "save.txt";
    protected string path => Application.persistentDataPath + fileName;

    public abstract void Save(GameData data);
    public abstract GameData Load();
}