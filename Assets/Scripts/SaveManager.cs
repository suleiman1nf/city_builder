﻿using System;
using UnityEngine;

public class SaveManager : MonoBehaviour
{

    private GameManager gameManager;

    private SaveController saveController;
    public SaveController SaveController => saveController ??= new BinnarySave();

    public event Action<GameData> OnLoad;

    public void Init(GameManager gameManager)
    {
        this.gameManager = gameManager;
    }
    
    public void Save()
    {
        if (gameManager == null)
        {
            Debug.LogError("Game manager is not initialized");
        }
        SaveController.Save(gameManager.GameData);
    }

    public void Load()
    {
        OnLoad?.Invoke(SaveController.Load());
    }
}