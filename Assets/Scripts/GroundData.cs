﻿using System;
using System.Collections.Generic;

[Serializable]
public class GroundData
{
    public List<TileData> tilesList;
    public TileData[,] tilesArray;
}