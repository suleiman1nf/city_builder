﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class BinnarySave : SaveController
{
    
    public override void Save(GameData data)
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        
        FileStream stream = new FileStream(path, FileMode.Create);
        binaryFormatter.Serialize(stream, data);
        stream.Close();
    }

    private bool IsSaveExists()
    {
        return File.Exists(path);
    }

    public override GameData Load()
    {
        if (!IsSaveExists())
        {
            Debug.LogError("Save file doesn't exist");
            return null;
        }

        BinaryFormatter formatter = new BinaryFormatter();
        
        FileStream stream = new FileStream(path, FileMode.Open);
        GameData gameData = formatter.Deserialize(stream) as GameData;
        stream.Close();
        return gameData;
    }
}