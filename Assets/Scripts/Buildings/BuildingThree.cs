﻿using System.Collections.Generic;
using UnityEngine;

public class BuildingThree : BuildingContainer
{
    public override string ResourceName()
    {
        return "3";
    }

    public override Vector3 GetPosition(List<TileData> tiles)
    {
        return new Vector3(tiles[1].Position.x,
            1.5f, tiles[2].Position.z);
    }

    protected override List<TileData> CheckTiles(int i, int j, TileData[,] tiles)
    {
        List<TileData> vec = new List<TileData>();
        int sizeX = tiles.GetLength(0);
        int sizeZ = tiles.GetLength(1);
        
        if (i + 2 >= sizeX || j + 2 >= sizeZ)
            return null;
        
        return new List<TileData>
        {
            tiles[i, j], tiles[i + 1, j], tiles[i, j + 1],
            tiles[i + 1, j + 1], tiles[i+2,j], tiles[i+2,j+1], tiles[i+1,j+2], tiles[i+2, j+2], tiles[i, j+2]
        };
    }
}