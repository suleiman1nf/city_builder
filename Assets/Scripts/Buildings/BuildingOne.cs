﻿using System.Collections.Generic;
using UnityEngine;

public class BuildingOne : BuildingContainer
{
    public override string ResourceName()
    {
        return "1";
    }

    public override Vector3 GetPosition(List<TileData> tiles)
    {
        return tiles[0].Position;
    }

    // public override BuildingData TryPlace(TileData currTile, TileData[,] tiles)
    // {
    //     BuildingData buildingData = new BuildingData();
    //     buildingData.Position = GetPosition(new List<TileData> {currTile});
    //     buildingData.resourcesName = ResourceName();
    //     buildingData.tiles = new List<TileData> {currTile};
    //     buildingData.Rotation = Vector3.zero;
    //     
    //     if(currTile.CanBuild())
    //         return buildingData;
    //     else
    //     {
    //         return null;
    //     }
    // }

    protected override List<TileData> CheckTiles(int i, int j, TileData[,] tiles)
    {
        return new List<TileData>{tiles[i,j]};
    }
}