﻿using System.Collections.Generic;
using UnityEngine;

public class BuildingTwo : BuildingContainer
{
    public override string ResourceName()
    {
        return "2";
    }

    public override Vector3 GetPosition(List<TileData> tiles)
    {
        return new Vector3(
            (tiles[0].Position.x + tiles[1].Position.x) / 2,
            1.5f,
            (tiles[0].Position.z + tiles[2].Position.z) / 2);
    }

    // public override BuildingData TryPlace(TileData currTile, TileData[,] tiles)
    // {
    //     var coordinates = tiles.CoordinatesOf(currTile);
    //     int sizeX = tiles.GetLength(0);
    //     int sizeZ = tiles.GetLength(1);
    //     int i = coordinates.i;
    //     int j = coordinates.j;
    //
    //     BuildingData buildingData = new BuildingData();
    //     var tileDatas = new List<TileData> {tiles[i,j], tiles[i+1,j], tiles[i, j+1], tiles[i+1, j+1]};
    //     buildingData.Position = GetPosition(tileDatas);
    //     buildingData.resourcesName = ResourceName();
    //     buildingData.tiles = tileDatas;
    //     buildingData.Rotation = Vector3.zero;;
    //     
    //     if (i + 1 >= sizeX || j + 1 >= sizeZ)
    //         return null;
    //
    //     if (tiles[i, j].CanBuild() && tiles[i + 1, j].CanBuild() && tiles[i, j + 1].CanBuild() &&
    //         tiles[i + 1, j + 1].CanBuild())
    //     {
    //         foreach (var tile in tiles)
    //         {
    //             tile.hasBuilding = true;
    //         }
    //         return buildingData;
    //     }
    //     else
    //     {
    //         return null;
    //     }
    // }

    protected override List<TileData> CheckTiles(int i, int j, TileData[,] tiles)
    {
        int sizeX = tiles.GetLength(0);
        int sizeZ = tiles.GetLength(1);
        
        if (i + 1 >= sizeX || j + 1 >= sizeZ)
            return null;
        
        return new List<TileData>
        {
            tiles[i, j], tiles[i + 1, j], tiles[i, j + 1],
            tiles[i + 1, j + 1]
        };
    }
}