﻿using System.Collections.Generic;
using UnityEngine;

public abstract class BuildingContainer
{
    public abstract string ResourceName();

    public abstract Vector3 GetPosition(List<TileData> tiles);

    public BuildingData TryPlace(TileData currTile, TileData[,] tiles)
    {
        var coordinates = tiles.CoordinatesOf(currTile);
        int sizeX = tiles.GetLength(0);
        int sizeZ = tiles.GetLength(1);
        int i = coordinates.i;
        int j = coordinates.j;

        List<TileData> checkTiles = CheckTiles(i,j,tiles);
        
        if (checkTiles != null && CanBuild(checkTiles))
        {
            BuildingData buildingData = new BuildingData();

            buildingData.Position = GetPosition(checkTiles);
            buildingData.resourcesName = ResourceName();
            buildingData.tiles = checkTiles;
            buildingData.Rotation = Vector3.zero;;
            
            foreach (var tile in checkTiles)
            {
                tile.hasBuilding = true;
            }

            return buildingData;
        }

        return null;
    }

    protected abstract List<TileData> CheckTiles(int i, int j, TileData[,] tiles);

    private bool CanBuild(List<TileData> tiles)
    {
        foreach (var tile in tiles)
        {
            if (!tile.CanBuild())
            {
                return false;
            }
        }

        return true;
    }
}