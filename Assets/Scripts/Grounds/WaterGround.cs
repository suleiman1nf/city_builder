﻿public class WaterGround : Ground, IGroundTransformer
{
    public TileType TransformType()
    {
        return TileType.Swamp;
    }

    public TileData TerraformerTileData()
    {
        return tile;
    }
}