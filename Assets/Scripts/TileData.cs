﻿using System;
using UnityEngine;

public enum TileType
{
    Grass,
    Sand,
    Water,
    Swamp,
}

[Serializable]
public class TileData
{
    private string resourcesName;
    private TileType tileType;
    public bool hasBuilding;
    public bool isBuildable;
    [SerializeField] private float[] position;
    [SerializeField] private float[] rotation;

    public Vector3 Position
    {
        get => new Vector3(position[0], position[1], position[2]);
        set => position = value.ToArray();
    }
    
    public Vector3 Rotation
    {
        get => new Vector3(rotation[0], rotation[1], rotation[2]);
        set => rotation = value.ToArray();
    }

    public bool CanBuild()
    {
        return isBuildable && !hasBuilding;
    }

    public string ResourcesName => resourcesName;

    public TileType TileType
    {
        get => tileType;
        set
        {
            tileType = value;
            switch (tileType)
            {
                case TileType.Grass:
                    isBuildable = true;
                    resourcesName = "Grass";
                    break;
                case TileType.Sand:
                    isBuildable = true;
                    resourcesName = "Sand";
                    break;
                case TileType.Swamp:
                    isBuildable = false;
                    resourcesName = "Swamp";
                    break;
                case TileType.Water:
                    isBuildable = false;
                    resourcesName = "Water";
                    break;
            }
        }
    }
}