﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

public class BuildingsGenerator
{
    private readonly float percent;

    public BuildingsGenerator(float percent)
    {
        this.percent = percent;
    }
    
    public List<BuildingData> Generate(GroundData groundData)
    {
        List<BuildingData> buildingDatas = new List<BuildingData>();
        
        List<TileData> tiles = new List<TileData>();
        foreach (var tileData in groundData.tilesList)
        {
            tiles.Add(tileData);
        }
        
        //Shuffle list
        Random random = new Random();
        tiles = tiles.OrderBy(a => random.Next()).ToList();

        int buildableTilesCount = tiles.Count(tile => tile.isBuildable);
        int expectedBuildingsCount = Mathf.FloorToInt(buildableTilesCount * percent);
        int currBuildingsCount = 0;

        foreach (var tileData in tiles)
        {
            if (tileData.isBuildable)
            {
                tileData.hasBuilding = true;
                currBuildingsCount++;
                if (currBuildingsCount >= expectedBuildingsCount)
                    break;
            }
        }

        foreach (var tile in tiles)
        {
            tile.hasBuilding = !tile.hasBuilding;
        }

        List<TileData> buildedTiles = new List<TileData>();

        var tilesArray = new TileData[groundData.tilesArray.GetLength(0),groundData.tilesArray.GetLength(1)];
        for (int i = 0; i < groundData.tilesArray.GetLength(0); i++)
        {
            for (int j = 0; j < groundData.tilesArray.GetLength(1); j++)
            {
                tilesArray[i,j] = groundData.tilesArray[i, j];
            }
        }
        
        for (int i = 0; i < tilesArray.GetLength(0) - 1; i++)
        {
            for (int j = 0; j < tilesArray.GetLength(1) - 1; j++)
            {
                BuildingData buildingData = BuildingsFactory.TryBuild<BuildingTwo>(tilesArray[i, j], tilesArray);
                if (buildingData != null)
                {
                    buildingDatas.Add(buildingData);
                    buildedTiles.AddRange(buildingData.tiles);
                }
            }    
        }
        
        for (int i = 0; i < tilesArray.GetLength(0); i++)
        {
            for (int j = 0; j < tilesArray.GetLength(1); j++)
            {
                BuildingData buildingData = BuildingsFactory.TryBuild<BuildingOne>(tilesArray[i, j], tilesArray);
                if (buildingData != null)
                {
                    buildingDatas.Add(buildingData);
                    buildedTiles.AddRange(buildingData.tiles);
                }
            }
        }

        foreach (var tile in tiles)
        {
            if (!buildedTiles.Exists((t)=>tile==t))
            {
                tile.hasBuilding = false;
            }
        }

        return buildingDatas;
    }
}