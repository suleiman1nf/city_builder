﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BuildingData
{
    public string resourcesName;
    [SerializeField] private float[] position;
    [SerializeField] private float[] rotation;
    public List<TileData> tiles;

    public Vector3 Position
    {
        get => new Vector3(position[0], position[1], position[2]);
        set => position = value.ToArray();
    }
    
    public Vector3 Rotation
    {
        get => new Vector3(rotation[0], rotation[1], rotation[2]);
        set => rotation = value.ToArray();
    }

}