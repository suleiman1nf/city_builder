﻿using System;
using System.Collections.Generic;

[Serializable]
public class GameData
{
    public GroundData groundData;
    public List<BuildingData> buildingData;
}