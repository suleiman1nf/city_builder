using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISelectable
{
    public void OnSelect();
    public void OnDeselect();
}

public class SelectManager : MonoBehaviour
{
    public static SelectManager Instance;
    private ISelectable currSelectedObject;

    private void Awake()
    {
        Instance = this;
    }

    public void DeselectLast()
    {
        SelectObject(null);
    }
    
    public void SelectObject(ISelectable selectable)
    {
        if (currSelectedObject != null && !currSelectedObject.Equals(null))
        {
            currSelectedObject?.OnDeselect();   
        }
        selectable?.OnSelect();
        currSelectedObject = selectable;
    }
}
