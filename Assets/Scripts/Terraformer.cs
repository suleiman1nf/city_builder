﻿using UnityEngine;

public class Terraformer : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private GroundPlacer groundPlacer;

    private bool isActive = false;

    public void ChangeState()
    {
        isActive = !isActive;
    }

    public void Update()
    {
        if (!isActive)
            return;
        
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        
            if (Physics.Raycast(ray, out hit)) {
                Transform objectHit = hit.transform;
                var ground = objectHit.GetComponent<IGroundTransformer>();
                if (ground != null)
                {
                    TileData tile = ground.TerraformerTileData();
                    tile.TileType = ground.TransformType();
                    groundPlacer.Create(tile);
                    isActive = false;
                    Destroy(objectHit.gameObject);
                }
            }    
        }
    }
}