﻿using System.Collections.Generic;
using UnityEngine;

public class Builder : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private BuildingsPlacer placer;
    [SerializeField] private GameManager gameManager;

    private bool isActive = false;
    private string currType;


    public void Deactivate()
    {
        isActive = false;
    }

    public void Activate(string type)
    {
        isActive = true;
        currType = type;
    }

    private BuildingData Build(TileData tile)
    {
        switch (currType)
        {
            case "1":
                return BuildingsFactory.TryBuild<BuildingOne>(tile, gameManager.GameData.groundData.tilesArray);
            case "2":
                return BuildingsFactory.TryBuild<BuildingTwo>(tile, gameManager.GameData.groundData.tilesArray);
            case "3":
                return BuildingsFactory.TryBuild<BuildingThree>(tile, gameManager.GameData.groundData.tilesArray);
            default:
                return null;
        }
    }

    public void Update()
    {
        if (!isActive)
            return;
        
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        
            if (Physics.Raycast(ray, out hit)) {
                Transform objectHit = hit.transform;
                var ground = objectHit.GetComponent<Ground>();
                if (ground != null)
                {
                    TileData tile = ground.tile;
                    var building = Build(tile);
                    if (building != null)
                    {
                        Deactivate();
                        placer.AddNewBuilding(building);
                    }
                }
            }    
        }
    }
}