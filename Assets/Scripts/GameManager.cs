using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GeneratorBehaviour generatorBehaviour;
    [SerializeField] private GroundPlacer groundPlacer;
    [SerializeField] private BuildingsGeneratorBehaviour buildingsGeneratorBehaviour;
    [SerializeField] private BuildingsPlacer buildingsPlacer;
    [SerializeField] private SaveManager saveManager;
    
    private GameData gameData;
    public GameData GameData => gameData;

    public void Start()
    {
        saveManager.Init(this);
        saveManager.OnLoad += OnSaveFileLoaded;
        
        buildingsPlacer.Init(this);
        GenerateLevel();
    }

    private void GenerateLevel()
    {
        var data = generatorBehaviour.Generate();
        
        groundPlacer.Place(data.tilesList);
        
        var buildings = buildingsGeneratorBehaviour.Generate(data);
        
        buildingsPlacer.Place(buildings);

        gameData = new GameData
        {
            buildingData = buildings,
            groundData = data,
        };
    }
    private void OnSaveFileLoaded(GameData data)
    {
        gameData = data;
        groundPlacer.Place(data.groundData.tilesList);
        buildingsPlacer.Place(data.buildingData);
    }
}
