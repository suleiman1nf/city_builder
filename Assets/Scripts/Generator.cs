﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Generator
{
    private int sizeX;
    private int sizeZ;
    private float offsetX;
    private float offsetZ;
    
    private float waterPercent;
    private float swampPercent;


    public Generator(int sizeX, int sizeZ, float offsetX, float offsetZ, float waterPercent, float swampPercent)
    {
        this.sizeX = sizeX;
        this.sizeZ = sizeZ;
        this.offsetX = offsetX;
        this.offsetZ = offsetZ;
        this.waterPercent = waterPercent;
        this.swampPercent = swampPercent;
    }
    
    public GroundData Generate()
    {
        List<TileData> tilesList = new List<TileData>();
        TileData[,] tiles = new TileData[sizeX,sizeZ];
        GroundData groundData = new GroundData {tilesArray = tiles, tilesList = tilesList};
        
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeZ; j++)
            {
                var tileData = new TileData
                {
                    TileType = TileType.Sand,
                    Position = new Vector3(offsetX * i, 0, offsetZ * j), 
                    Rotation = Vector3.zero
                };
                tilesList.Add(tileData);
                tiles[i, j] = tileData;
            }
        }

        var random = new System.Random();

        int waterCount = Mathf.FloorToInt(waterPercent * tilesList.Count);
        int swampCount = Mathf.FloorToInt(swampPercent * tilesList.Count);
        var restPercent = 1 - waterPercent - swampPercent;
        float grassPercent = restPercent - UnityEngine.Random.Range(0, restPercent);
        int grassCount = Mathf.FloorToInt(grassPercent * tilesList.Count);
        int sandCount = tilesList.Count - waterCount - swampCount - grassCount;
        
        List<TileType> tileTypes = new List<TileType>();
        for(int i = 0; i < waterCount; i++)
        {
            tileTypes.Add(TileType.Water);
        }
        for(int i = 0; i < swampCount; i++)
        {
            tileTypes.Add(TileType.Swamp);
        }
        for(int i = 0; i < grassCount; i++)
        {
            tileTypes.Add(TileType.Grass);
        }
        for(int i = 0; i < sandCount; i++)
        {
            tileTypes.Add(TileType.Sand);
        }
        
        if(tileTypes.Count != tilesList.Count)
            Debug.LogError("Tyles count != types count, check tilesTypes array");
        
        //Shuffle list
        tileTypes = tileTypes.OrderBy(a => random.Next()).ToList();

        for(int i = 0; i < tilesList.Count; i++)
        {
            tilesList[i].TileType = tileTypes[i];
        }

        return groundData;
    }
}