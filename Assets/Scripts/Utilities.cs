﻿using UnityEngine;

public static class Utilities
{
    public static float[] ToArray(this Vector3 vec)
    {
        var arr = new float[3];
        arr[0] = vec.x;
        arr[1] = vec.y;
        arr[2] = vec.z;

        return arr;
    }
    
    public static (int i, int j) CoordinatesOf<T>(this T[,] matrix, T value)
    {
        int w = matrix.GetLength(0); // width
        int h = matrix.GetLength(1); // height

        for (int x = 0; x < w; ++x)
        {
            for (int y = 0; y < h; ++y)
            {
                if (matrix[x, y].Equals(value))
                    return (x, y);
            }
        }

        return (-1, -1);
    }
}