﻿using System.Collections.Generic;
using UnityEngine;

public class BuildingsPlacer : MonoBehaviour
{
    [SerializeField] private Transform buildingsParent;

    private string resourcesPath = "Buildings/";

    private GameManager gameManager;

    public void Init(GameManager gameManager)
    {
        this.gameManager = gameManager;
    }
    
    public void Place(List<BuildingData> buildings)
    {
        foreach (Transform child in buildingsParent)
        {
            Destroy(child.gameObject);
        }
        
        foreach (var building in buildings)
        {
            Create(building);
        }
    }

    private void Create(BuildingData building)
    {
        GameObject prefab = Resources.Load<GameObject>(resourcesPath + building.resourcesName);
        GameObject go = Instantiate(prefab, buildingsParent);
        go.transform.localPosition = building.Position;
        go.transform.localEulerAngles = building.Rotation;
        go.GetComponent<Building>().Init(building, this);
    }

    public void DeleteBuilding(Building building, BuildingData buildingData)
    {
        Destroy(building.gameObject);
        foreach (var tile in buildingData.tiles)
        {
            tile.hasBuilding = false;
        }
    }

    public void AddNewBuilding(BuildingData buildingData)
    {
        Create(buildingData);
        gameManager.GameData.buildingData.Add(buildingData);
    }
}