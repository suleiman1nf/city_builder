﻿using System;
using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Building : MonoBehaviour, ISelectable
{
    private float scale = 1.5f;
    private Vector3 startScale;

    private BuildingData data;
    private BuildingsPlacer buildingsPlacer;

    public void Init(BuildingData buildingData, BuildingsPlacer buildingsPlacer)
    {
        data = buildingData;
        this.buildingsPlacer = buildingsPlacer;
    }
    
    private void Awake()
    {
        startScale = transform.localScale;
    }

    public void OnMouseDown()
    {
        SelectManager.Instance.SelectObject(this);
    }

    public void OnSelect()
    {
        buildingsPlacer.DeleteBuilding(this,data);
        transform.DOScale(startScale * scale, 0.3f);
    }

    public void OnDeselect()
    {
        transform.DOScale(startScale, 0.3f);
    }
}