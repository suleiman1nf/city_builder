﻿using UnityEngine;

public interface IGroundTransformer
{
    public TileType TransformType();
    public TileData TerraformerTileData();
}