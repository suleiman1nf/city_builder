﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

public class BuildingsGeneratorBehaviour : MonoBehaviour
{
    [Range(0f,1f)]
    [SerializeField] private float percent;

    private BuildingsGenerator buildingsGenerator;

    public List<BuildingData> Generate(GroundData groundData)
    {
        buildingsGenerator ??= new BuildingsGenerator(percent);
        
        return buildingsGenerator.Generate(groundData);
    }
}