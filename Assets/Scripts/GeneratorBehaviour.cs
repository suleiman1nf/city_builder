﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GeneratorBehaviour : MonoBehaviour
{
    [SerializeField] private int sizeX;
    [SerializeField] private int sizeZ;
    [SerializeField] private float offsetX;
    [SerializeField] private float offsetZ;

    [Space]
    [Range(0f,1f)]
    [SerializeField] private float waterPercent;
    [Range(0f,1f)]
    [SerializeField] private float swampPercent;

    private Generator generator;

    private float WaterPercent
    {
        get => waterPercent;
        set => waterPercent = Mathf.Clamp(value, 0f, 1-swampPercent);
    }

    private float SwampPercent
    {
        get => swampPercent;
        set => swampPercent = Mathf.Clamp(value, 0f, 1-waterPercent);
    }

    private void OnValidate()
    {
        WaterPercent = waterPercent;
        SwampPercent = swampPercent;
    }

    public GroundData Generate()
    {
        generator ??= new Generator(sizeX, sizeZ, offsetX, offsetZ, waterPercent, swampPercent);
        return generator.Generate();
    }
}