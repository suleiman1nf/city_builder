﻿using System.Collections.Generic;
using UnityEngine;

public static class BuildingsFactory
{
    public static BuildingData TryBuild<T>(TileData currTile, TileData[,] tiles) where T : BuildingContainer, new()
    {
        T buildingContainer = new T();
        BuildingData buildingData = buildingContainer.TryPlace(currTile, tiles);
        return buildingData;
    }
}